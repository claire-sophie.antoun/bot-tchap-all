### French Version below ###


# Matrix Bot

This project contains a bot designed to operate within the Matrix protocol. The bot can perform various tasks, including gathering data from Metabase and BigBlueButton. It can also set room descriptions and schedule automatic messages.

## Features

### Data Bot (Metabase)
- Fetch and send data concerning specific services.
- Send automated messages with the data at scheduled times.
- Responds to various commands such as `!data apps`, `!data visio`, `!data tubes`, `!data codimd`, and `!data help`.

### Visio Bot (BigBlueButton)
- Create and manage conference links.
- Responds to `!visio` command to create or retrieve a conference link.
- Responds to `!desc` command to set the room description with the conference link.

## Dependencies
- `os`, `re`, `time`, `random`, `hashlib`, `asyncio`, `requests`, `xml.etree.ElementTree`
- `simplematrixbotlib` for Matrix bot functionality.
- `bigbluebutton_api_python` for BigBlueButton integration.

## Configuration

Configure the bot by editing the `/src/config.py.example` module that you will need to rename `config.py`. Some of the important configurations include:
- Matrix home server, bot name, and password.
- Metabase username and password.
- BigBlueButton URL and secret.
- Scheduled time for sending automatic messages.
- Language and translations.

## How It Works

### Data Bot Functionality

1. **Help Command**: Responds to `!data help` with a list of available commands.
2. **Specific Data Commands**: Responds to various data-specific commands with the information retrieved from a defined dashboard.

### Video Conferencing Functionality

1. **Create Conference Link**: Creates a conference link for a specific meeting.
2. **Handle 'visio' Command**: Responds to `!visio` with a conference link.
3. **Handle 'desc' Command**: Responds to `!desc` by setting the room description including the conference link.

### Scheduled Messages

If `data_bot` is enabled, it schedules auto messages to send data at specified times using `APScheduler`.

## Usage

1. Ensure all dependencies are installed and configuration is set.
2. Run the script to initialize the bot.
3. The bot will respond to the commands in the configured Matrix rooms.

## Installation

### Requirements
- Python 3.6 or higher
- Virtual environment (recommended)

### Steps
1. Clone the repository or download the source code.
2. Navigate to the project directory.
3. Create a virtual environment (optional but recommended):
   ```bash
   python3 -m venv myenv
   source myenv/bin/activate  # On Windows, use `myenv\Scripts\activate`
   ```
4. Install the required packages:
   ```bash
   pip install simplematrixbotlib bigbluebutton_api_python requests APScheduler
   ```
5. Configure the bot by updating the `config.py` file with the required details, such as Matrix server credentials, BigBlueButton URL and secret, data service URL, etc.

## Usage

### Start the Bot
1. Navigate to the project directory where your bot code is located.
2. If you created a virtual environment earlier, activate it:
   ```bash
   source myenv/bin/activate  # On Windows, use `myenv\Scripts\activate`
   ```
3. Run the bot:
   ```bash
   python bot.py
   ```

### French Version ####

# Matrix Bot

Ce projet contient un bot conçu pour fonctionner avec le protocole Matrix. Le bot peut effectuer diverses tâches, y compris récupérer des données de Metabase et BigBlueButton. Il peut également définir les descriptions de salle et planifier des messages automatiques.

## Fonctionnalités

### Bot de Données (Metabase)
- Récupérer et envoyer des données concernant des services spécifiques.
- Envoyer des messages automatiques avec les données aux horaires programmés.
- Répond aux différentes commandes comme `!data apps`, `!data visio`, `!data tubes`, `!data codimd`, et `!data help`.

### Bot Visio (BigBlueButton)
- Créer et gérer les liens de conférence.
- Répond à la commande `!visio` pour créer ou récupérer un lien de conférence.
- Répond à la commande `!desc` pour définir la description de la salle avec le lien de conférence.

## Dépendances
- `os`, `re`, `time`, `random`, `hashlib`, `asyncio`, `requests`, `xml.etree.ElementTree`
- `simplematrixbotlib` pour la fonctionnalité de bot Matrix.
- `bigbluebutton_api_python` pour l'intégration de BigBlueButton.

## Configuration

Configurez le bot en modifiant le module `/src/config.py.example` qu'il faudra renommer `config.py`.
Certaines des configurations importantes incluent :
- Serveur domestique Matrix, nom du bot, et mot de passe.
- Nom d'utilisateur et mot de passe Metabase.
- URL BigBlueButton et secret.
- Horaire prévu pour l'envoi de messages automatiques.
- Langue et traductions.

## Comment ça marche

### Fonctionnalité du Bot de Données

1. **Commande d'Aide**: Répond à `!data help` avec une liste des commandes disponibles.
2. **Commandes de Données Spécifiques**: Répond à diverses commandes spécifiques aux données avec les informations récupérées à partir d'un tableau de bord.

### Fonctionnalité de Visioconférence

1. **Créer un Lien de Conférence**: Crée un lien de conférence pour une réunion spécifique.
2. **Gérer la Commande 'visio'**: Répond à `!visio` avec un lien de conférence.
3. **Gérer la Commande 'desc'**: Répond à `!desc` en changeant la description de la salle pour y inclure le lien de conférence.

### Messages Planifiés

Si `data_bot` est activé, il planifie des messages automatiques pour envoyer des données à des heures spécifiées en utilisant `APScheduler`.

## Utilisation

1. Assurez-vous que toutes les dépendances sont installées et que la configuration est définie.
2. Exécutez le script pour initialiser le bot.
3. Le bot répondra aux commandes dans les salles Matrix configurées.

## Installation

### Exigences
- Python 3.6 ou supérieur
- Environnement virtuel (recommandé)

### Étapes
1. Clonez le dépôt ou téléchargez le code source.
2. Naviguez jusqu'au répertoire du projet.
3. Créez un environnement virtuel (facultatif mais recommandé) :
   ```bash
   python3 -m venv myenv
   source myenv/bin/activate  # Sous Windows, utilisez `myenv\Scripts\activate`
   ```
4. Installez les paquets requis :
   ```bash
   pip install simplematrixbotlib bigbluebutton_api_python requests APScheduler
   ```
5. Configurez le bot en mettant à jour le fichier `config.py` avec les détails requis, tels que les identifiants du serveur Matrix, l'URL et le secret de BigBlueButton, l'URL du service de données, etc.

## Utilisation

### Démarrer le Bot
1. Naviguez jusqu'au répertoire du projet où se trouve le code de votre bot.
2. Si vous avez créé un environnement virtuel plus tôt, activez-le :
   ```bash
   source myenv/bin/activate  # Sous Windows, utilisez `myenv\Scripts\activate`
   ```
3. Lancez le bot :
   ```bash
   python bot.py
   ```
```
