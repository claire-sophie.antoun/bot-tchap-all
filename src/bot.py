import os
import re
import time
import random
import hashlib
import asyncio
import requests
import xml.etree.ElementTree as ET
import simplematrixbotlib as botlib
from hashlib import sha1
from urllib.parse import urlencode
from bigbluebutton_api_python import BigBlueButton
import trad
import config
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from config import (
    send_time_hour, send_time_minute,
    bot_name, bot_password, homeserver,
    command_filters, room_filters,
    base_url, dashboard_id, metabase_username, metabase_password,
    bbb_url, bbb_secret, language
)

# Translations
success_msg = trad.translations[language]['success_msg']
need_admin_msg = trad.translations[language]['need_admin_msg']
failed_to_set_description = trad.translations[config.language]['failed_to_set_description']
failed_to_generate_link = trad.translations[language]['failed_to_generate_link']

PREFIX = '!'

creds = botlib.Creds(
    homeserver,
    bot_name,
    bot_password,
)
bot = botlib.Bot(creds)


def get_metabase_session_token():
    response = requests.post(
        f'{base_url}/api/session',
        json={'username': metabase_username , 'password': metabase_password } 
    )
    response.raise_for_status()
    return response.json()['id']

def get_matrix_session_token():
    login_url = f"{homeserver}/_matrix/client/r0/login"
    data = {
        "type": "m.login.password",
        "identifier": {"type": "m.id.user", "user": bot_name},
        "password": bot_password,
    }

    response = requests.post(login_url, json=data)
    response.raise_for_status()
    return response.json().get("access_token")



def get_joined_room_id(session_token):
    url = f"{homeserver}/_matrix/client/v3/joined_rooms"
    headers = {'Accept': 'application/json'}
    params = {'access_token': session_token}

    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        joined_rooms = data.get('joined_rooms', [])
        return joined_rooms
    else:
        print(f"Request error: {response.status_code}")
        return []


def get_room_names(room_id, session_token):
    url = f"{homeserver}/_matrix/client/v3/rooms/{room_id}/aliases"
    headers = {"Authorization": f"Bearer {session_token}", "Accept": "application/json"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        data = response.json()
        aliases = data.get("aliases", [])
        return aliases[0] if aliases else None
    except requests.exceptions.RequestException as e:
        print(f"Error fetching room aliases for {room_id}: {e}")
        return None


def get_dashboard_data(session_token):
    url = f'{base_url}/api/dashboard/{dashboard_id}'
    response = requests.get(
        url,
        headers={'X-Metabase-Session': session_token}
    )
    response.raise_for_status()
    return response.json()


def extract_question_data(dashboard_data, filters):
    question_data = []
    cards = dashboard_data.get('ordered_cards', [])
    for card in cards:
        card_info = card.get('card')
        question_id = card_info.get('id')
        question_title = card_info.get('name')
        if question_id is not None and question_title is not None:
            for filter_value in filters:
                if filter_value in question_title:
                    question_data.append((question_title, question_id))
                    break
    return question_data

def get_question_count(session_token, question_id):
    response = requests.get(
        f'{base_url}/api/card/{question_id}',
        headers={'X-Metabase-Session': session_token}
    )
    try:
        data = response.json()
        count = int(data['result_metadata'][0]['fingerprint']['type']['type/Number']['avg'])
        return count
    except (KeyError, IndexError, ValueError):
        return None



def set_room_description(session_token, room_id, description):
    url = f"{homeserver}/_matrix/client/r0/rooms/{room_id}/state/m.room.topic"
    headers = {
        'Authorization': f'Bearer {session_token}',
        'Content-Type': 'application/json'
    }
    data = {'topic': description}

    response = requests.put(url, json=data, headers=headers)
    if response.status_code == 403:
        error_response = response.json()
        if error_response.get("errcode") == "M_FORBIDDEN" and error_response.get("error") == "You don't have permission to post that to the room. user_level (0) < send_level (100)":
            return (False , need_admin_msg)
    elif response.status_code != 200:
        print(f"Failed to set room description. Status Code: {response.status_code}. Response: {response.text}")
        failure_msg= translations['en']['failed_to_set_description'].format(status_code=response.status_code, response_text=response.text)
        return (False,failure_msg)
    return (True, success_msg)

# Dictionary to store the conference links for each room
conference_links = {}


def create_conference_link(meeting_id, meeting_name):
    print(f"Creating conference link for meeting_id: {meeting_id}, meeting_name: {meeting_name}") # Debug print

    # Parameters for meeting creation
    create_params = {
        'allowStartStopRecording': 'true',
        'autoStartRecording': 'false',
        'meetingID': meeting_id,
        'moderatorPW': 'moderator',
        'name': meeting_name,
        'record': 'false',
    }
    create_params_encoded = urlencode(create_params)

    # Calculate the checksum for creating the meeting
    create_checksum_string = f"create{create_params_encoded}{bbb_secret}"
    create_checksum = sha1(create_checksum_string.encode()).hexdigest()

    # Create the meeting creation URL
    create_url = f"{bbb_url}/create?{create_params_encoded}&checksum={create_checksum}"

    # Create the meeting by making a GET request to the create_url
    create_response = requests.get(create_url)

    # Parse the XML response to check the return code
    response_xml = ET.fromstring(create_response.text)
    returncode = response_xml.find("returncode").text
    message_key = response_xml.find("messageKey").text if response_xml.find("messageKey") is not None else None

    # Check creation response or running status
    if returncode in ['SUCCESS', 'duplicateWarning'] or message_key == 'idNotUnique':
        # Generate a join URL for everyone as a moderator
        join_params = f"fullName=Moderator&meetingID={meeting_id}&password=moderator&redirect=true"
        checksum_string = f"join{join_params}{bbb_secret}"
        checksum = sha1(checksum_string.encode()).hexdigest()
        join_url = f"{bbb_url}/join?{join_params}&checksum={checksum}"

        print("Moderator join URL is :", join_url)
        conference_links[meeting_id] = join_url
        return join_url
    else:
        print(f"Failed to create meeting for room ID: {meeting_id}, meeting name: {meeting_name}")
        return None


@bot.listener.on_message_event
async def handle_message(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)
    session_token = get_matrix_session_token()
    room_name = room.display_name
    conference_link = None


    if match.is_not_from_this_bot() and match.prefix():
        content = message.body.strip()

        if data_bot == 1:
            # Handle data bot's commands
            if content == "!data help":
                await bot.api.send_text_message(room.room_id, "Bonjour, je suis en bot qui vous envoie des data concernant les services de apps. Voici les commandes auxquels je réponds.\n!data apps\n!data visio\n!data tubes\n!data codimd et !data help")
            else:
                if content in command_filters:
                    command = content
                    filters = command_filters[command]

                    session_token = get_metabase_session_token()
                    dashboard_data = get_dashboard_data(session_token)

                    question_data = extract_question_data(dashboard_data, filters)

                    answers = []
                    for question_title, question_id in question_data:
                        count = get_question_count(session_token, question_id)
                        if count is not None:
                            answers.append(f"{question_title}: {count}")

                    if len(answers) > 0:
                        await bot.api.send_text_message(room.room_id, '\n'.join(answers))
                        print(f"Message sent to room {room.room_id}")

        if visio_bot == 1:
            # Handle BigBlueButton bot's commands
            if room.room_id in conference_links:
                    conference_link = conference_links[room.room_id]
            else:
                    conference_link = create_conference_link(room.room_id, room_name)

            welcome_msg = trad.translations[language]['welcome_msg'].format(conference_link=conference_link)
            description = trad.translations[language]['Description'].format(conference_link=conference_link, room_name=room_name)

            # Check for the specific 'visio' command
            if content == f"{PREFIX}visio":
                if conference_link is not None:
                    await bot.api.send_text_message(room.room_id, welcome_msg)
                    print(f"Responded to 'visio' command in room {room.room_id}")
                else:
                    await bot.api.send_text_message(room.room_id, failed_to_generate_link)

            # Check for the specific 'desc' command
            elif content == f"{PREFIX}desc":
                success, message = set_room_description(session_token, room.room_id, description)
                await bot.api.send_text_message(room.room_id, message)
                print(f"Responded to 'desc' command in room {room.room_id}")


if data_bot == 1:
    scheduler = AsyncIOScheduler()

    def schedule_auto_messages():
        scheduler = AsyncIOScheduler()
        scheduler.add_job(send_auto_messages, 'cron', hour=send_time_hour, minute=send_time_minute)
        scheduler.start()


    async def send_auto_messages():
        await auto_messages()

    async def auto_messages():
        print("called function")
        metabase_session_token = get_metabase_session_token()
        matrix_session_token = get_matrix_session_token(bot_name, bot_password, homeserver)
        dashboard_data = get_dashboard_data(metabase_session_token)
        joined_rooms = get_joined_room_id(matrix_session_token)

        print(f"Joined Rooms: {joined_rooms}")

        for joined_room_id in joined_rooms:
            room_name = get_room_names(joined_room_id, matrix_session_token)
            if room_name:
                print(f"Room ID: {joined_room_id}, Room Name: {room_name}")

                for command, filters in room_filters.items():
                    if command in room_name:
                        print(f"Room matches filter: {command}")

                        question_data = extract_question_data(dashboard_data, filters)

                        answers = []
                        for question_title, question_id in question_data:
                            count = get_question_count(metabase_session_token, question_id)
                            if count is not None:
                                answers.append(f"{question_title}: {count}")

                        if len(answers) > 0:
                            try:
                                print("Sending message...")
                                await bot.api.send_text_message(joined_room_id, f"!data {command}\n" + '\n'.join(answers))
                                print("Message sent!")
                            except Exception as e:
                                print(f"Error sending message: {e}")


if __name__ == "__main__":
    os.system("rm session.txt | rm -rf store")
    if data_bot == 1:
        schedule_auto_messages()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(bot.main())
    loop.close()